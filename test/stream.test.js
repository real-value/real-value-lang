import { describe } from 'riteway'
import { Model } from '../index.js'
import Infrastructure from 'real-value-infrastructure-base'
import { WritableAdapter } from 'real-value-stream-adapter'
import { makeAsyncArrayGenerator } from 'funprog'
import { ChannelFactory } from 'real-value-channel-arrayqueue'

const debug = require('debug')('test')

function makeCounter () {
  let count = 0
  return {
    total: () => count,
    adder: x => {
      // console.log(x)
      count++
    }
  }
}

async function * agenerator () {
  let count = 10000
  let i = 0
  while (i < count) {
  // while(true){
    i++
    debug(i)
    yield '' + i
    await new Promise((resolve) => setTimeout(() => resolve(), 0))
  }
}

describe('channel hypercore', async (assert) => {
  let cnt = 0
  let ChannelFactory = require('real-value-channel-hypercore')
  let channelFactory = ChannelFactory('ram')
  let channel = channelFactory()
  let model = Model()
  let s1 = model.from([1, 2, 3]).log().toChannel({ channel })
  let s2 = model.fromChannel({ channel }).tap(x => { cnt = cnt + 1 })
  model.run(() => {
    assert({
      given: 'hypercore channel',
      should: 'pass events through the channel',
      actual: `${cnt}`,
      expected: '3'
    })
  })
})

describe('toArray', async (assert) => {
  let cnt = 0
  let model = Model()
  let result = model.from('1,2,3')
    // .log()
    .toArray()
  await model.complete()
  assert({
    given: 'array from node',
    should: 'contain events',
    actual: `${result.length}`,
    expected: '3'
  })
})

describe('fromCallback', async (assert) => {
  let buffer = []
  let model = Model()
  let { producer, stream } = model.fromCallback()
  stream.tap(x => buffer.push(x))
  model.run(() => {
    assert({
      given: 'the fromCallback',
      should: 'captured events produced',
      actual: `${buffer.length}`,
      expected: '2'
    })
  })
  producer(1)
  producer(2)
  producer(null)
})

describe('Get readable from stream', async (assert) => {
  let model = Model()
  let readable = model.from([1, 2, 3]).readable()

  let cnt = 0
  model.from(readable).tap(x => {
    cnt++
  })
  model.run(() => {
    assert({
      given: 'a streams duplex',
      should: 'be able to from a new stream',
      actual: `${cnt}`,
      expected: '3'
    })
  })
})

describe('Use Variables As Ouput', async (assert) => {
  let variables = {}
  let model = Model({ variables })
  model.from([{ a: 1, b: 1 }, { a: 2, b: 2 }])
  .to('test.var')
  model.run(() => {
    assert({
      given: 'the source stream',
      should: 'be in the variables',
      actual: `${variables['test.var'].length}`,
      expected: '2'
    })
  })
})

describe('toWritable', async (assert) => {
  let model = Model()
  let f = model.from('1,2')
  let cnt = 0
  let add = (x) => {
    debug(`add ${x}`)
    cnt++
  }
  let done = () => {
    debug('done')
  }
  let writableadapter = WritableAdapter({ add, done })
  f.toWritable(writableadapter)
  model.run(() => {
    assert({
      given: 'something',
      should: 'work',
      actual: `${cnt}`,
      expected: '2'
    })
  })
})

let sampleReducer = (h, n) => {
  let m = parseInt(n)
  if (m >= 10) {
    return { reduced: 0, downstream: [h + m] }
  } else {
    return { reduced: parseInt(h + m), downstream: [] }
  }
}

let sampleReduceFinish = async (downstream, historical) => {
  // await new Promise((resolve)=>setTimeout(resolve,1000))
  downstream(historical)
}

// describe('Investigate using reduce in place of batch', async (assert) => {
//   let model = Model()
//   // model.from([{ batch: [1, 2, 3] }]).log().each({ itemsFn: x => x.batch })
//   model.from('1,2,3,4')
//     .reduce((h, n) => {
//       // console.log(h)
//       h.push(n)
//       // console.log(h)
//       return { reduced: h }
//     }, [], (downstream, h) => downstream(h))
//     .tap(x => console.log(x))
//     .end((downstream) => downstream(1))
//   model.run(() => {
//     assert({
//       given: 'something',
//       should: 'work',
//       actual: `2`,
//       expected: '2'
//     })
//   })
// })

describe('Source', async (assert) => {
  let scenarios = [
    {
      expression: (model) => { let s1 = model.from('1,2,3'); let s2 = model.from('1,3,4'); return s1.difference(s2, { keyFn: x => x }) },
      expected: {
        given: 'stream of values',
        should: 'return a stream of 2 value (2 and 4',
        result: 'length 2'
      }
    },
    {
      expression: (model) => model.from('1,1,1').end((downstream) => downstream(1)),
      expected: {
        given: 'stream of values',
        should: 'return a stream',
        result: 'length 4'
      }
    },
    {

      expression: (model) => model.from([{ a: 1, b: 1 }]).to('foo.var'),
      expected: {
        given: 'stream of values',
        should: 'return a stream',
        result: 'length 1'
      }
    },
    {
      expression: (model) => model.from([{ a: 1, b: 1 }]).to('foo.var'),
      expected: {
        given: 'stream of values',
        should: 'return a stream',
        result: 'length 1'
      }
    },
    {
      expression: (model) => { model.from('1,2').toChannel({ name: 'channel1' }); return model.fromChannel({ name: 'channel1', yieldTotal: 2 }).log() },
      expected: {
        given: 'stream of values through a channel',
        should: 'return a stream with those values',
        result: 'length 2'
      }
    },
    {
      expression: (model) => model.from([{ batch: [1, 2, 3] }]).each({ itemsFn: x => x.batch }),
      expected: {
        given: 'each batch',
        should: 'return all batch items',
        result: 'length 3'
      }
    },
    {
      expression: (model) => model.from('test/data.csv').buffer(5),
      expected: {
        given: 'stream and buffer large than data set',
        should: 'only buffer events',
        result: 'length 0'
      }
    },
    {
      expression: (model) => model.from('test/data.csv').buffer(3, 50/* pipe read delay */),
      expected: {
        given: 'stream and buffer smaller than data set',
        should: 'return all data',
        result: 'length 4'
      }
    },
    {
      expression: (model) => model.from('test/data.csv').take(2, { delay: 50/* pipe read delay */ }), // eslint-disable-line 
      expected: {
        given: 'stream and take',
        should: 'return a stream',
        result: 'length 2'
      }
    },
    {
      expression: (model) => model.from('1,2,3,4').skip(2, { delay: 50/* pipe read delay */ }), // eslint-disable-line 
      expected: {
        given: 'stream and take',
        should: 'return a stream',
        result: 'length 2'
      }
    },
    {
      expression: (model) => model.from('AssetId,SerialNo\n1,ABC'),
      expected: {
        given: 'stream from generator',
        should: 'return a stream',
        result: 'length 1'
      }
    },
    {
      expression: (model) => model.from('1,,,3'),
       expected: {
        given: 'stream of values',
        should: 'return a stream',
        result: 'length 4'
      }
    },
    {
      expression: (model) => model.from('1,2,,3').log(50/* pipe read delay */),
      expected: {
        given: 'log of a stream of values',
        should: 'return a stream',
        result: 'length 4'
      }
    },
    {
      expression: (model) => model.from('1,2,,3').filter(x => parseInt(x) % 2 === 0, {
        include: true,
        pipeReadDelay: 50
      }),
      expected: {
        given: 'filter of stream values',
        should: 'return a filtered stream',
        result: 'length 1'
      }
    },
    {
      expression: (model) => {
        let s1 = model.from('1,2')
        let s2 = model.from('3,4')
        return s1.merge(s2)
      },
      expected: {
        given: 'merge stream values',
        should: 'return a merged stream',
        result: 'length 4'
      }
    },
    {
      expression: (model) => {
        let s1 = model.from([{ 'key': 1, 'b': 1 }, { 'key': 2, 'value': 2 }])
        let s2 = model.from([{ 'key': 1, 'c': 1 }, { 'key': 3, 'value': 2 }])
        return s1.join(s2)
      },
      expected: {
        given: 'join stream values',
        should: 'return a join stream',
        result: 'length 3'
      }
    },
    {
      expression: (model) => model.from([{ 'key': 1, 'value': 1 }, { 'key': 2, 'value': 2 }, { 'key': 1, 'value': 1 }, { 'key': 1, 'value': 'one' }]).table({ pipeReadDelay: 50 }),
      expected: {
        given: 'table pipeline',
        should: 'return table stream',
        result: 'length 3'
      }
    },
    {
      // only: true,
      expression: (model) => {
        let s1 = model.from([1, 2])
        let s2 = model.from([3, 4])
        return s1.combine(s2)
      },
      expected: {
        given: 'two arrays of length 2',
        should: 'combine should return',
        result: 'length 4'
      }
    },
    {
      // only: true,
      expression: (model) => model.from('1,2,3,4').map(x => x, { pipeReadDelay: 50 }),
      expected: {
        given: 'stream of values with a pipe read delay',
        should: 'return a stream',
        result: 'length 4'
      }
    },
    {

      expression: (model) => model.from('1,2,3,4').map(x => `${x}\n`).to('./tmp/somefile.csv', { firstline: 'Col', debugonly: true }),
      expected: {
        given: 'stream of values',
        should: 'return a stream',
        result: 'length 4'
      }
    },
    {
      expression: (model) => model.from([{ key: 1, value: 1 }, { key: 2, value: 2 }, { key: 3, value: 3 }]).asCSV(),
      expected: {
        given: 'stream of values asCSV',
        should: 'the stream is produced',
        result: 'length 4' // as the header is included
      }
    },
    // {
    //   expression: (model) => model.from(['line1\n', 'line2']).toAzure({
    //     filename: 'Test.csv',
    //     account: 'projectcontrolsstorage',
    //     accountKey: process.env.ACCESS_KEY || '',
    //     shareName: 'projectcontrols',
    //     directoryName: `ReportingDataSet`
    //   }, { debugOnly: true }),
    //   expected: {
    //     given: 'stream of values asCSV',
    //     should: 'the stream is produced',
    //     result: 'length 2' // as the header is included
    //   }
    // },
    {
      expression: (model) => model.from('1,2').toChannel({ name: 'test' }).log(),
      expected: {
        given: 'log of a stream of values',
        should: 'return a stream',
        result: 'length 2'
      }
    },
    {
      expression: (model) => model.from('1,2').passthrough(),
      expected: {
        given: 'passthrough',
        should: 'return a stream',
        result: 'length 2'
      }
    },
    {
      // only: true,
      expression: (model) => model.from('1,2,3').delay(50, 10/* pipe read delay */),
      expected: {
        given: 'stream with delay',
        should: 'return a stream',
        result: 'length 3'
      }
    },
    {
      expression: (model) => model.from(makeAsyncArrayGenerator([1, 2, 3], {})),
      expected: {
        given: 'stream from iterable',
        should: 'return a stream',
        result: 'length 3'
      }
    },
    {
      // only: true,
      expression: (model) => model.from([{ type: '1', value: 1 }, { type: '2', value: 2 }, { type: '1', value: 3 }, { type: '2', value: 4 }]).batch(),
      expected: {
        given: 'stream batch',
        should: 'return 2 batches',
        result: 'length 2'
      }
    }
  ]

  let selected = scenarios.filter(x => x.only)
  if (selected.length === 0) selected = scenarios
  for (const index in selected) {
    let scenario = selected[index]

    let amodel = Model({ channelFactory: ChannelFactory() })
    let stream = scenario.expression(amodel)

    let { total, adder } = makeCounter()
    stream
      // .log()
      .observe(adder, 50/* pipe read delay */)

    // Under test
    amodel.run(() => {
      assert({
        given: scenario.expected.given,
        should: scenario.expected.should,
        actual: `length ${total()}`,
        expected: scenario.expected.result
      })
    })
    // await new Promise((resolve) => setTimeout(() => resolve(), 200))
  }
})

describe('When batch is destroyed, end of stream is signaled', async (assert) => {
  let cnt = 0
  let model = Model()
  let s1 = model.from('test/test.csv')
  .filter(x => x.key === 1)
  .batch({ typeFn: x => '1', delay: 1000, maxbatch: 5000 })
  .log()
  model.run(() => {
    assert({
      given: 'batch in a pipeline that produces no events',
      should: 'the pipeline terminate',
      actual: `true`,
      expected: 'true'
    })
  })
})
