import { describe } from 'riteway'
import { Model } from '../index.js'
import { ChannelFactory } from 'real-value-channel-arrayqueue'
const debug = require('debug')('test')

const Topology = require('real-value-topology')

describe('Model complete', async (assert) => {
  {
    let cnt = 0
    let amodel = Model()
    amodel.from('1,2,3').delay(500).tap(x => cnt++)
    await amodel.complete()
    assert({
      given: 'a model with delayed processing',
      should: 'be able to complete',
      actual: `${cnt}`,
      expected: '3'
    })
  }
})

describe('Model getNode', async (assert) => {
  {
    let amodel = Model()
    amodel.from('1,2,3').filter().log()

    let filter = amodel.getNode('filter')
    let log = amodel.getNode('log')
    let foobar = amodel.getNode('foobar')

    assert({
      given: 'a model with some node',
      should: 'be able to access nodes',
      actual: `${filter != null} ${log != null} ${foobar == null}`,
      expected: 'true true true'
    })
  }
})

describe('Single Stream Node', async (assert) => {
  {
    let amodel = Model()
    amodel.filter()

    let topology = Topology()
    topology.traverseModel(amodel)

    topology.render()

    assert({
      given: 'a model and a from(csv)',
      should: 'the graph should be',
      actual: `${topology.render()}`,
      expected: '\tfilter'
    })
  }
})

describe('Stream fromChannel', async (assert) => {
  {
    let amodel = Model({ channelFactory: ChannelFactory() })
    let stream = amodel.fromChannel({ name: 'testChannel' })

    stream.log()
    let topology = Topology()
    topology.traverseModel(amodel)

    topology.render()

    assert({
      given: 'a model and a fromChannel()',
      should: 'the graph should be',
      actual: `${topology.render()}`,
      expected: '\ttestChannel\tlog'
    })
  }
})

describe('Stream from callback', async (assert) => {
  {
    let amodel = Model()
    let { producer, stream } = amodel.fromCallback()

    stream.log()
    let topology = Topology()
    topology.traverseModel(amodel)

    topology.render()

    assert({
      given: 'a model and a fromCallback()',
      should: 'the graph should be',
      actual: `${topology.render()}`,
      expected: '\tfromCallback\tlog'
    })
  }
})

describe('Stream Generator', async (assert) => {
  let amodel = Model()
  amodel.from(function * () { yield 1 })

  let topology = Topology()
  topology.traverseModel(amodel)

  assert({
    given: 'a model and a from(generator)',
    should: 'the graph should be',
    actual: `${topology.render()}`,
    expected: '\tfromGen'
  })
})

describe('Simple Linear Stream', async (assert) => {
  let amodel = Model()
  amodel.from('1,,,3', { name: 'csv1' }).log()

  let topology = Topology()
  topology.traverseModel(amodel)

  assert({
    given: 'a model and a from(csv)',
    should: 'the graph should be',
    actual: `${topology.render()}`,
    expected: '\tcsv1\tlog'
  })
})

describe('Linear Stream', async (assert) => {
    let amodel = Model()
    amodel.from('1,,,3', { name: 'csv1' }).tap((x) => x).log()

    let topology = Topology()
    topology.traverseModel(amodel)

    assert({
      given: 'a model and a from(csv)',
      should: 'the graph should be',
      actual: `${topology.render()}`,
      expected: '\tcsv1\ttap\tlog'
    })
})

describe('Multiple Streams', async (assert) => {
    let amodel = Model()

    amodel.from('1,2,3').log()
    amodel.from('1,2,3').log()

    let topology = Topology()
    topology.traverseModel(amodel)

    assert({
      given: 'a model and 2 fromCsv',
      should: 'the graph should be',
      actual: `${topology.render()}`,
      expected: '\tfromStream\tlog\n\tfromStream\tlog'
    })
})

describe('From CSV File', async (assert) => {
  let amodel = Model()
  amodel.from('test/test.csv').tap((x) => x).log()

  let topology = Topology()
  topology.traverseModel(amodel)

  assert({
    given: 'a model and a from(csv file path)',
    should: 'the graph should be',
    actual: `${topology.render()}`,
    expected: '\tfromCSV\ttap\tlog'
  })
})

describe('Merge Streams', async (assert) => {
  let amodel = Model()

  let s1 = amodel.from('1,2,3', { name: 'fromcsv1' })
  let s2 = amodel.from('4,5,6', { name: 'fromcsv2' })

  s1.merge(s2).log()

  let topology = Topology()
  topology.traverseModel(amodel)

  assert({
    given: 'a model and 2 merged stream',
    should: 'be the graph',
    actual: `${topology.render()}`,
    expected: '\tfromcsv1\tmerge1\tfromGen\tlog\n\tfromcsv2\tmerge2'
  })
})

describe('Join Streams', async (assert) => {
  let amodel = Model()

  let s1 = amodel.from([{ k: 1, value1: 1 }])
  let s2 = amodel.from([{ k: 1, value2: 1 }])
  let s3 = s1.join(s2)

  let topology = Topology()
  topology.traverseModel(amodel)

  assert({
    given: 'a model and a split stream',
    should: 'the graph should be',
    actual: `${topology.render()}`,
    expected: '\tfromArray\tjoin1\tfromGen\n\tfromArray\tjoin2'
  })
})

describe('Stream To', async (assert) => {
  {
    let amodel = Model()
    amodel.from('1,2,3').to('/tmp/out.txt')

    let topology = Topology()
    topology.traverseModel(amodel)
    topology.render()

    assert({
      given: 'a model with a from and to',
      should: 'the graph should be',
      actual: `${topology.render()}`,
      expected: '\tfromStream\tto'
    })
  }
})

describe('Passthrough', async (assert) => {
  {
    let amodel = Model()
    amodel.from('1,2,3').passthrough().log()

    let topology = Topology()
    topology.traverseModel(amodel)
    topology.render()

    assert({
      given: 'a model with a passhthrough',
      should: 'the graph should be',
      actual: `${topology.render()}`,
      expected: '\tfromStream\tpassthrough\tlog'
    })
  }
})

describe('Take from Stream', async (assert) => {
  let amodel = Model()
  amodel.from('1,2,3').take(1)

  let topology = Topology()
  topology.traverseModel(amodel)

  assert({
    given: 'a model and a from(csv) and take',
    should: 'the graph should be',
    actual: `${topology.render()}`,
    expected: '\tfromStream\ttake'
  })
})

describe('Batch Stream', async (assert) => {
  let amodel = Model()
  amodel.from('1,2,3').batch(100, 2)

  let topology = Topology()
  topology.traverseModel(amodel)

  assert({
    given: 'a model and a from(csv) and batch',
    should: 'the graph should be',
    actual: `${topology.render()}`,
    expected: '\tfromStream\tbatch'
  })
})

describe('Each from Stream', async (assert) => {
  let amodel = Model()
  amodel.from([{ batch: [1, 2, 3] }]).each()
  let topology = Topology()
  topology.traverseModel(amodel)
  assert({
    given: 'a batch',
    should: 'each batch item',
    actual: `${topology.render()}`,
    expected: '\tfromArray\teach'
  })
})
