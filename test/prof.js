const { Model } = require('../index.js')
const debug = require('debug')('test')

async function * agenerator () {
    let count = 100000
    let i = 0
    while (i < count) {
    // while(true){
      i++
      debug(i)
      yield '' + i
      // await new Promise((resolve) => setTimeout(() => resolve(), 0))
    }
  }

function doit () {
    let amodel = Model({ highWaterMark: 1000 })
    let g = agenerator()
    let stream = amodel.from(g)
    .filter(x => x % 2)
    .to('/tmp/file.out')
    amodel.run()
}

doit()
