
const Tutorials = [
    {
        d: 'A data stream can be a set of values. The simplest example is a common separated set of values.',
        l: ',1,,,,2,,3',
        x: "model.from('1,2')"
    }, {
        d: 'A data stream can be an array of Json objects.',
        x: 'model.from([{a:1},{a:2}])'
    }, {
        d: 'A data stream can be inspected (see the developer tools console output)',
        x: "model.from('1,2').log()"
    }, {
        d: 'A data stream is delayed in time',
        l: ',1,2,,3 => ,,,1,2,,3',
        x: "model.from('1,2').delay(1000).log()"
    }, {
        d: 'Filter a data stream',
        l: '1,2,3,4 => 1,3',
        x: "model.from('1,2,3,4').filter(x=>x%2)"
    }, {
        d: 'Take some number of datum from a data stream',
        l: '1,2,3,4 => 1,2',
        x: "model.from('1,2,3,4').take(2)"
    }, {
        d: 'Skip some number of datum from a data stream',
        l: '1,2,3,4 => 3,4',
        x: "model.from('1,2,3,4').skip(2)"
    }, {
        d: 'Map values in a data stream',
        l: '1,2,3 => 2,4,6',
        x: "model.from('1,2,3,4').map(x=>2*x)"
    }, {
        d: 'Apply function to values in a data stream without changing the stream',
        l: '1,2 => f(1),f(2)',
        x: "model.from('1,2,3,4').tap(x=>alert(x))"
    }, {
        d: 'Split a data stream',
        l: '1,2 => 2,4\n    => 1',
        x: "let s1 = model.from('1,2,3,4')\nlet s2=s1;s1.map(x=>2*x).tap(capture1)\ns2.filter(x=>x%2).tap(capture2)",
        multiresults: true
    }, {
        d: 'Merge 2 stream',
        l: '1,2 => 1,2,3,4\n3,4 =>',
        x: "let s1 = model.from('1,2')\nlet s2=model.from('3,4')\ns1.merge(s2)"
    }, {
        d: 'Outer join of 2 streams (given a join key)',
        l: '{K:1 b:1},{K:2 b:2} => {K:1 b:1 c1},{K:2 b:2 c2}\n{K:1 c:1},{K:2 c:2} =>',
        x: "let s1 = model.from([{ 'key': 1, 'b': 1 }, { 'key': 2, 'value': 2 }])\nlet s2 = model.from([{ 'key': 1, 'c': 1 }, { 'key': 3, 'value': 3}])\ns1.join(s2,{type:'outer'})"
    }, {
        d: 'Inner join of 2 streams (given a join key)',
        x: "let s1 = model.from([{ 'key': 1, 'b': 1 }, { 'key': 2, 'value': 2 }])\nlet s2 = model.from([{ 'key': 1, 'c': 1 }, { 'key': 3, 'value': 3}])\ns1.join(s2,{type:'inner'})"
    }, {
        d: 'Left join of 2 streams',
        x: "let s1 = model.from([{ 'key': 1, 'b': 1 }, { 'key': 2, 'value': 2 }])\nlet s2 = model.from([{ 'key': 1, 'c': 1 }, { 'key': 3, 'value': 3}])\ns1.join(s2,{type:'left'})"
    }, {
        d: 'Right join of 2 streams',
        x: "let s1 = model.from([{ 'key': 1, 'b': 1 }, { 'key': 2, 'value': 2 }])\nlet s2 = model.from([{ 'key': 1, 'c': 1 }, { 'key': 3, 'value': 3}])\ns1.join(s2,{type:'right'})"
    }, {
        d: 'Batch stream events into batches (given a batch key)',
        l: '{K:1 b:1},{K:1 b:2} => [{type: 1, [{b:1}{b:2}]]',
        x: 'model.from([{type:1,value: 1},{type:2,value: 2},{type:1,value: 3}]).batch()'
    }, {
        d: 'Break apart stream events from batches (given a batch key)',
        l: '[{batch: [1,2,3]}] => 1,2,3',
        x: 'model.from([{batch:[1,2,3]}]).each({itemsFn: x=>x.batch})'
    }, {
        d: 'Limit a stream to some specific throughput',
        l: ',1,,,,2,,3,, -=> ,1,,,,1,1,1,1,1',
        x: "model.from('1,,2,1,3,,,).limitTo(1)",
        enabled: false
    }, {
        d: 'Reduce stream events',
        l: '1,1,1,1,1 => ,,2,,2,1',
        x: "model.from('1,1,1,1,1,1').reduce((h, n) => ({ reduced: h + parseInt(n), downstream: [] }))"
    }
]

module.exports = { Tutorials }
