
const Nodes = [
    "model.from('')",
    "model.to('')",
    'model.tap()',
    'model.merge()',
    'model.map()',
    'model.filter()',
    'model.join()',
    'model.table()'
]

module.exports = { Nodes }
