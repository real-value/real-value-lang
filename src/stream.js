const debug = require('debug')('stream')

const { Readable } = require('stream')
const { ReadableAdapter, DuplexAdapter, WritableAdapter } = require('real-value-stream-adapter')

let ID_SEQUENCE = 0

const CONTINUE_STREAM_PROCESSING = true // Used to signal to upstream source nodes that there is no point to produce more events

let DUPLEX_READ_DELAY = 50

let fs = null

module.exports = function (themodel) {
    try {
        fs = require('fs')
    } catch (err) {
        // doesnt work in the browser
    }

    let model = themodel

    function create (options = {}) {
        let name = options.name
        let description = options.description
        let rationale = options.rationale
        let type = options.type
        let theSource = []
        let meta = options.meta
        let theSink = []
        let event = options.event
        let finished = false
        let eventSource = options.eventSource
        let thetype = options.type
        let leafNode = true
        let pipeEnabled = options.pipeEnabled
        let duplex = null
        if (options.source) {
            theSource = options.source
        }

        let sourceGenerator = null

        debug(`Creating stream node [${name}]`)

        let thestream = {
            uid: ID_SEQUENCE++,
            id,
            name,
            description,
            rationale,
            type,
            pipeEnabled,
            source,
            sink,
            length () {
                return meta.length
            },
            leafNode,
            addSinkOf,
            event,

            asCSV,

            from: createFrom,
            toWritable,
            to,
            toArray,
            log,
            observe,
            tap,
            each,
            merge,
            filter,
            join,
            difference,
            combine,
            table,
            map,
            delay,
            take,
            skip,
            buffer,
            batch,
            passthrough,
            reduce,
            end,

            toChannel,
            readable
        }
        if (type !== 'source') { model.register(thestream) }

        thestream.onDeregister = (callback, downstream) => { callback() }
        thestream.finalCallback = (downstream) => {
                debug(`${thestream.name} final`)
                if (leafNode) {
                    thestream.onDeregister(() => {
                        debug(`${thestream.name} deregister`)
                        model.deregister(thestream)
                    }, downstream)
                }
            }
        thestream.destroyCallback = (downstream) => {
                debug(`${thestream.name} destroy`)
                if (!leafNode) {
                    thestream.onDeregister(() => {
                        debug(`${thestream.name} deregister`)
                        model.deregister(thestream)
                    }, downstream)
                }
            }

        thestream.done = () => debug(`${thestream.name} done`)

        function id () {
            return this.uid
        }
        function source () {
            return theSource
        }
        function sink () {
            return theSink
        }
        function addSinkOf (streamNode) {
            sink().push(streamNode)
            debug(`${name} -> ${streamNode.name}`)
            streamNode.leafNode = false
        }
        function readable () {
            if (thestream.duplex instanceof Readable) { return thestream.duplex }
            if (thestream.iterator) return ReadableAdapter(thestream.iterator)
            throw Error('Attempt to create readable with readable or iterator available')
        }
        function observe (f, delay) {
            debug('observe')
            return tap(f, delay)
        }
        function tap (f, pipeReadDelay) {
            debug('tap')
            let newstream = createTap()
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    try {
                    model.infrastructure().tap(f, x, downstream, callback)
                    } catch (err) {
                        // console.log('here')
                    }
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            newstream.source = [ thestream ]
            addSinkOf(newstream)
            return newstream
        }
        function take (count, options = {}) {
            const operation = 'take'
            debug(operation)
            let { delay = DUPLEX_READ_DELAY } = options
            let newstream = createTake()
            newstream.source = [ thestream ]
            let taker = model.infrastructure().take(count)
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    taker(x, downstream, callback)
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function skip (count, options = {}) {
            debug('skip')
            let { delay = DUPLEX_READ_DELAY } = options
            let newstream = createSkip()
            newstream.source = [ thestream ]
            let skipper = model.infrastructure().skip(count)
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    skipper(x, downstream, callback)
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function each (options = {}) {
            debug('each')
            let { itemsFn = x => x } = options
            let newstream = createEach()
            newstream.source = [ thestream ]
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (event, depth, downstream, callback) => {
                    let items = itemsFn(event)
                    debug(`Items ${items.length}`)
                    items.forEach((x) => {
                        debug(`Downstream ${x}`)
                        downstream(x)
                    })
                    callback()
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function buffer (count, delay = DUPLEX_READ_DELAY) {
            debug('buffer')
            let newstream = createBuffer()
            newstream.source = [ thestream ]
            let bufferer = model.infrastructure().buffer(count)
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    bufferer(x, downstream, callback)
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: delay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function filter (f, options = {}) {
            debug('filter')
            let { include = true, pipeReadDelay = DUPLEX_READ_DELAY } = options
            let newstream = createFilter()
            newstream.source = [ thestream ]
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    model.infrastructure().filter(f, include, x, downstream, callback)
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function log (pipeReadDelay = DUPLEX_READ_DELAY) {
            debug('log')
            let newstream = createLog()
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    model.infrastructure().tap(console.log, x, downstream, callback)
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            newstream.source = [ thestream ]
            addSinkOf(newstream)
            return newstream
        }
        function merge (stream2, options = {}) {
            debug('merge')
            let merge1stream = createMerge('merge1')
            merge1stream.source = [ thestream ]

            let done1 = false
            let done2 = false
            let buffer = []
            merge1stream.duplex = WritableAdapter({
                add: x => {
                    // console.log(x)
                    buffer.push(x)
                },
                done: () => { done1 = true; model.deregister(merge1stream) },
                highWaterMark: model.highWaterMark
            }).writable
            thestream.addSinkOf(merge1stream)

            let merge2stream = createMerge('merge2')
            merge2stream.duplex = WritableAdapter({
                add: x => {
                    // console.log(x)
                    buffer.push(x)
                },
                done: () => { done2 = true; model.deregister(merge2stream) },
                highWaterMark: model.highWaterMark
            }).writable
            stream2.addSinkOf(merge2stream)

            let generator = async function * () {
                while (true) {
                    if (buffer.length > 0) {
                        debug(`yield`)
                        yield buffer.shift()
                    } else {
                        if (!done1 || !done2) {
                            await new Promise((resolve) => { setTimeout(() => { resolve() }, 1000) })
                        } else {
                            debug(`merge done`)
                            return
                        }
                    }
              }
            }
            let sourcestream = createFromGenerator(generator, options)
            merge1stream.addSinkOf(sourcestream)
            merge2stream.addSinkOf(sourcestream)
            model.addSource(sourcestream)
            return sourcestream
        }
        function join (stream2, options = {}) {
            debug('join')
            let { keyFn, joinFn } = options
            let join1stream = createJoin('join1')
            join1stream.source = [ thestream ]
            let { join1, join2, flush } = model.infrastructure().join(options)

            let done1 = false
            let done2 = false
            let buffer = []
            let downstream = x => {
                buffer.push(x)
            }
            join1stream.duplex = WritableAdapter({
                add: x => {
                    join1(x, downstream, () => {})
                },
                done: () => { done1 = true; model.deregister(join1stream); if (done1 && done2) flush(downstream) },
                highWaterMark: model.highWaterMark
            }).writable
            thestream.addSinkOf(join1stream)

            let join2stream = createJoin('join2')
            join2stream.duplex = WritableAdapter({
                add: x => {
                    join2(x, downstream, () => {})
                },
                done: () => { done2 = true; model.deregister(join2stream); if (done1 && done2) flush(downstream) },
                highWaterMark: model.highWaterMark
            }).writable
            stream2.addSinkOf(join2stream)

            let generator = async function * () {
                while (true) {
                    if (buffer.length > 0) {
                        debug(`yield`)
                        yield buffer.shift()
                    } else {
                        if (!done1 || !done2) {
                            await new Promise((resolve) => { setTimeout(() => { resolve() }, 1000) })
                        } else {
                            debug(`join done`)
                            return
                        }
                    }
              }
            }
            let sourcestream = createFromGenerator(generator)
            join1stream.addSinkOf(sourcestream)
            join2stream.addSinkOf(sourcestream)
            model.addSource(sourcestream)
            return sourcestream
        }
        function combine (stream2, options = {}) {
            debug('combine')
            let { combineFn } = options
            let combine1stream = createCombine('combine1')
            combine1stream.source = [ thestream ]
            let { combine1, combine2, flush } = model.infrastructure().combine(options)
            let done1 = false
            let done2 = false
            let buffer = []
            let downstream = x => buffer.push(x)
            combine1stream.duplex = WritableAdapter({
                add: x => {
                    combine1(x, () => {})
                },
                done: () => { done1 = true; model.deregister(combine1stream); if (done1 && done2) flush(downstream) },
                highWaterMark: model.highWaterMark
            }).writable
            thestream.addSinkOf(combine1stream)

            let combine2stream = createCombine('combine2')
            combine2stream.duplex = WritableAdapter({
                add: x => {
                    combine2(x, () => {})
                },
                done: () => { done2 = true; model.deregister(combine2stream); if (done1 && done2) flush(downstream) },
                highWaterMark: model.highWaterMark
            }).writable
            stream2.addSinkOf(combine2stream)

            let generator = async function * () {
                while (true) {
                    if (buffer.length > 0) {
                        debug(`yield`)
                        yield buffer.shift()
                    } else {
                        if (!done1 || !done2) {
                            await new Promise((resolve) => { setTimeout(() => { resolve() }, 1000) })
                        } else {
                            debug(`combine done`)
                            return
                        }
                    }
              }
            }
            let sourcestream = createFromGenerator(generator)
            combine1stream.addSinkOf(sourcestream)
            combine2stream.addSinkOf(sourcestream)
            model.addSource(sourcestream)
            return sourcestream
        }
        function table (options = {}) {
            debug('table')
            const { key, reducer, pipeReadDelay = DUPLEX_READ_DELAY } = options
            let newstream = createTable('table')
            newstream.source = [ thestream ]
            let { get, process } = model.infrastructure().table({ key, reducer })
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    process(x, downstream, callback)
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function map (mapFunction, options = {}) {
            debug('map')
            let { pipeReadDelay = DUPLEX_READ_DELAY } = options
            let newstream = createMap('map')
            newstream.source = [ thestream ]
            newstream.duplex = DuplexAdapter({
                eventHandler: (x, depth, downstream, callback) => {
                    model.infrastructure().map(mapFunction, x, downstream, callback)
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function asCSV (content, options = {}) {
            debug('asCSV')
            let { pipeReadDelay = DUPLEX_READ_DELAY } = options
            let newstream = createMap('AsCSV')
            newstream.source = [ thestream ]
            let { eventHandler: asCSV, flush } = model.infrastructure().asCSV(content, options)
            newstream.onDeregister = (callback, downstream) => flush(callback, downstream)
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    asCSV(x, downstream, callback)
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function reduce (reducer = (h, n) => n, initial = 0, reduceFinish = (downstream, h) => { h && downstream(h) }) {
            debug('reducer')
            let { pipeReadDelay = DUPLEX_READ_DELAY } = options
            let newstream = createReduce()
            newstream.source = [ thestream ]
            let historical = initial
            let downstreamValues = []
            // newstream.onDeregister = async (callback, downstream) => {

            // }
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    let { reduced: r, downstream: ds } = reducer(historical, x)
                    if (Array.isArray(ds)) {
                        ds.forEach((d) => {
                            downstream(d)
                        })
                    } else if (ds) {
                        downstream(ds)
                    }
                    historical = r
                    callback()
                },
                done: newstream.done,
                finalCallback: (downstream) => {
                    if (reduceFinish) {
                        debug(`--------- ${reduceFinish}`)
                        reduceFinish(downstream, historical)
                    }
                    historical = null
                    newstream.finalCallback(downstream)
                },
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function end (onEnd = (downstream) => {}) {
            debug('end')
            let { pipeReadDelay = DUPLEX_READ_DELAY } = options
            let newstream = createEnd()
            newstream.source = [ thestream ]

            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    downstream(x)
                    callback()
                },
                done: newstream.done,
                finalCallback: (downstream) => {
                    onEnd(downstream)
                    newstream.finalCallback(downstream)
                },
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function difference (stream2, options = {}) {
            debug('difference')
            options.type = 'xor'
            options.joinFn = (x, y) => x || y
            return join(stream2, options)
            // let { keyFn, keyFn2 } = options

            // let diffstream = createDifference('diff')
            // diffstream.source = [ thestream ]

            // let { diff1, diff2, flush } = model.infrastructure().difference(options)
            // let done1 = false
            // let done2 = false
            // diffstream.duplex = DuplexAdapter({
            //     add: x => {
            //         diff1(x)
            //     },
            //     done: () => { done1 = true; model.deregister(diff1stream) },
            //     finalCallback: (downstream) => done1 && done2 && flush(downstream),
            //     highWaterMark: model.highWaterMark
            // }).writable
            // thestream.addSinkOf(diffstream)

            // let diff2stream = createJoin('diff2')
            // diff2stream.duplex = WritableAdapter({
            //     add: x => {
            //         diff2(x, downstream, () => {})
            //     },
            //     done: () => { done2 = true; model.deregister(join2stream) },
            //     finalCallback: (downstream) => done1 && done2 && flush(downstream),
            //     highWaterMark: model.highWaterMark
            // }).writable
            // stream2.addSinkOf(diff2stream)

            // return diffstream
        }
        function passthrough (options = {}) {
            debug('passthrough')
            let { pipeReadDelay = DUPLEX_READ_DELAY } = options
            let newstream = createPassthrough()
            newstream.source = [ thestream ]
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    downstream(x)
                    callback()
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function delay (delayAmount, pipeReadDelay = DUPLEX_READ_DELAY) {
            debug('delay')
            let newstream = createDelay()
            newstream.source = [ thestream ]
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: async (x, depth, downstream, callback) => {
                    await model.infrastructure().delay(delayAmount, x)
                    downstream(x)
                    callback()
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function batch (options = {}) {
            debug('batch')
            let { typeFn = x => x.type, addToBatcherFn = x => x, delay = 100, maxbatch = 100 } = options
            let newstream = createBatch()
            newstream.source = [ thestream ]
            let completed = false
            let { batchInProgress, batcher } = model.infrastructure().batch(typeFn, addToBatcherFn, delay, maxbatch, () => {
                debug('Batch completed')
                completed = true
            })
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    batcher(x, downstream, callback)
                },

                allProcessed: () => completed,
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: 50
            }).duplex

            newstream.onDeregister = async (callback, downstream) => {
                while (batchInProgress()) {
                    await new Promise((resolve) => setTimeout(resolve, 1000))
                }
                downstream(null)
                callback()
            }

            addSinkOf(newstream)
            return newstream
        }
        function toChannel (options = {}) {
            debug('toChannel')
            let { name = 'channel', pipeReadDelay = DUPLEX_READ_DELAY, channel = null } = options
            let newstream = createToChannel()
            newstream.source = [ thestream ]
            if (channel === null) { channel = model.channelFactory(name) }
            newstream.onDeregister = (callback, downstream) => channel.enqueue(null)
            newstream.duplex = DuplexAdapter({
                eventHandler: (x, depth, downstream, callback) => {
                    channel.enqueue(x)
                    downstream(x)
                    callback()
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark
            }).duplex
            addSinkOf(newstream)
            return newstream
        }
        function toWritable (writableadapter, options = {}) {
            debug('toWritable')
            let tostream = createMerge('to')
            writableadapter.setFinalCallback(() => {
                debug(`To final`)
                debug(`ToWritable shutdown `)
                model.deregister(tostream)
            })
            tostream.source = [ thestream ]
            tostream.duplex = writableadapter.writable
            addSinkOf(tostream)
            return tostream
        }
        function to (content, options = {}) {
            debug('to')
            let { pipeReadDelay = DUPLEX_READ_DELAY } = options
            let newstream = createTo()
            newstream.source = [ thestream ]
            let opts = Object.assign({ variables: model.variables }, options)
            let { eventHandler: toFile, flush } = model.infrastructure().toStream(content, opts)
            newstream.onDeregister = (callback) => {
                flush((numLines) => {
                    callback()
                })
            }
            newstream.duplex = DuplexAdapter({
                maxListeners: model.maxListeners,
                eventHandler: (x, depth, downstream, callback) => {
                    toFile(x, downstream, callback)
                },
                done: newstream.done,
                finalCallback: newstream.finalCallback,
                destroyCallback: newstream.destroyCallback,
                highWaterMark: model.highWaterMark,
                delay: pipeReadDelay
              }).duplex
            addSinkOf(newstream)
            return newstream
        }
        // function toAzure (content, options = {}) {
        //     debug('toAzure')
        //     let { pipeReadDelay = DUPLEX_READ_DELAY } = options
        //     let newstream = createTo('toAzure')
        //     newstream.source = [ thestream ]
        //     let { eventHandler: toAzure, flush } = model.infrastructure().toAzure(content, options)
        //     newstream.onDeregister = (callback) => flush(callback)
        //     newstream.duplex = DuplexAdapter({
        //         maxListeners: model.maxListeners,
        //         eventHandler: (x, depth, downstream, callback) => {
        //             toAzure(x, downstream, callback)
        //         },
        //         done: newstream.done,
        //         finalCallback: newstream.finalCallback,
        //         destroyCallback: newstream.destroyCallback,
        //         highWaterMark: model.highWaterMark,
        //         delay: pipeReadDelay
        //       }).duplex
        //     addSinkOf(newstream)
        //     return newstream
        // }
        function toArray (content, options = {}) {
            debug('toArray')
            let newstream = createToArray()
            newstream.source = [ thestream ]
            let buffer = []
            newstream.duplex = WritableAdapter({
                add: x => {
                    buffer.push(x)
                },
                done: () => {
                    model.deregister(newstream)
                },
                highWaterMark: model.highWaterMark
            }).writable

            addSinkOf(newstream)
            return buffer
        }
        return thestream
    }

    function createFromChannel (options = {}) {
        debug(`fromChannel`)
        let { name, channel = null } = options
        if (channel === null && model.channelFactory === null) {
            throw new Error('ChannelFactory needs to be defined')
        }
        if (channel === null) { channel = model.channelFactory(name) }
        let stream = createStreamFromChannel(options)
        let gen = model.infrastructure().createGeneratorFromChannel(channel, options)
        stream.iterator = gen()
        return stream
    }

    function createFromCallback (options) {
        debug(`fromCallback`)
        let { producer, readable } = model.infrastructure().fromCallback()
        let stream = createStreamFromCallback(options)
        stream.duplex = readable
        return {
            producer,
            stream
        }
    }

    function createFromLines (file, options = {}) {
        debug(`fromLines ${file}`)
        let stream = create({
            name: options.name ? options.name : 'fromLines',
            description: 'Produce a stream of lines from a file',
            rationale: 'This stream type has a low parse overhead when makes it good for large files',
            type: 'source',
            meta: {
            }
        })
        let opts = { variables: model.variables, ...options }

        let linestream = null
        if (file.endsWith('.var')) {
                let variables = options.variables
                linestream = new Readable()
                linestream.push(variables[file].trim())
                linestream.push(null)
        } else if (fs != null) {
            linestream = fs.createReadStream(file)
        }
        stream.iterator = model.infrastructure().fromLines(linestream, opts)
        return stream
    }

    function createFrom (content, options) {
        debug(`from`)
        if (content instanceof Readable) {
            return createFromReadable(content, options)
        } else if (Array.isArray(content)) {
            return createFromArray(content, options)
        } else if (typeof content === 'string') {
            if (content.endsWith('.var')) {
                debug('from var')
                let opts = Object.assign({ variables: model.variables }, options)
                return createFromCSV(content, opts)
            // } else if (content.endsWith('.xlsx')) {
            //     debug('from xlsx')
            //     return createFromXSLX(content, options)
            } else if (content.endsWith('.csv')) {
                debug('from csv')
                return createFromCSV(content, options)
            } else if (content.indexOf('\n') > 0) {
                debug('from csv')
                return createFromCSV(content, options)
            } else {
                try {
                    let json = JSON.parse(content)
                    return createFromJson(json)
                } catch (error) {
                    // console.log(error)
                    return createFromStream(content, options)
                }
            }
        } else if (typeof content === 'function') {
            return createFromGenerator(content, options)
        } else if (typeof content[Symbol.iterator] === 'function') {
            return createFromIterator(content, options)
        } else if (typeof content[Symbol.asyncIterator] === 'function') {
            return createFromIterator(content, options)
        }
        return null
    }

    function createFromCSV (file, options = {}) {
        debug(`fromCSV ${file}`)
        let stream = create({
            name: options.name ? options.name : 'fromCSV',
            description: 'Produce a stream of data from a csv file',
            rationale: 'CSV is a ubiquitous data source',
            type: 'source',
            meta: {
            }
        })
        let opts = Object.assign({ variables: model.variables }, options)
        stream.iterator = model.infrastructure().fromCSV(file, opts)
        return stream
    }

    // function createFromXSLX (file, options = {}) {
    //     debug(`fromXSLX`)
    //     let stream = create({
    //         name: options.name ? options.name : 'fromXSLX',
    //         description: 'Produce a stream of data from worksheet in a csv file',
    //         rationale: 'XLSX is a ubiquitous data source',
    //         type: 'source',
    //         meta: {
    //         }
    //     })
    //     stream.iterator = model.infrastructure().fromXLSX(stream, file)
    //     return stream
    // }

    function createFromReadable (readable, options = {}) {
        debug(`fromReadable`)
        let stream = create({
            name: options.name ? options.name : 'fromReadable',
            description: 'Produce a stream of data from a readable',
            rationale: 'Adapts a readable into a stream node',
            type: 'source',
            meta: {
            }
        })
        stream.duplex = readable
        return stream
    }

    function createFromArray (streamdata, options = {}) {
        debug(`fromArray`)
        let stream = create({
            name: options.name ? options.name : 'fromArray',
            description: 'Produce a stream of data from an array',
            rationale: 'Data can be sourced from json arrays',
            type: 'source',
            meta: {
                length: streamdata.length
            }
        })
        stream.iterator = model.infrastructure().fromArray(stream, streamdata)
        return stream
    }

    function createFromStream (content, options = {}) {
        debug(`fromStream`)
        let streamdata = null
        if (content.indexOf('\n') === -1) {
            streamdata = content.split(',')
        }
        let stream = create({
            name: options.name ? options.name : 'fromStream',
            description: 'Produce a stream of data from a stream of data',
            rationale: 'File streams are a good format for large data sets',
            type: 'source',
            meta: {
                length: streamdata.length
            }
        })
        stream.iterator = model.infrastructure().fromArray(stream, streamdata)
        return stream
    }

    function createFromJson (jsonContent, options = {}) {
        debug(`fromJson`)
        let streamdata = jsonContent
        let stream = create({
            name: options.name ? options.name : 'fromJson',
            description: 'Produce a stream of data from json',
            rationale: 'JSON is a native data source for javascript',
            type: 'source',
            meta: {
                length: streamdata.length
            }
        })
        let generator = model.infrastructure().fromArray(stream, streamdata)
        stream.iterator = generator()
        return stream
    }

    function createFromGenerator (generator, options = {}) {
        debug(`fromGenerator`)
        let stream = create({
            name: options.name ? options.name : 'fromGen',
            pipeEnabled: false,
            description: 'Produce a stream of data from a generator',
            rationale: 'Generators are easy ways to create large stream of data',
            type: 'source',
            meta: {
                length: -1
            }
        })
        stream.iterator = generator()
        return stream
    }
    function createFromIterator (iterator, options = {}) {
        debug(`fromIterator`)
        let stream = create({
            name: options.name ? options.name : 'fromIterator',
            description: 'Produce a stream of data an iterator',
            rationale: 'Iterators are easy ways to create large stream of data',
            type: 'source',
            meta: {
                length: -1
            }
        })
        stream.iterator = iterator
        return stream
    }
    function createStreamFromCallback (options = {}) {
        debug(`fromCallback`)
        let stream = create({
            name: options.name ? options.name : 'fromCallback',
            pipeEnabled: false,
            description: 'Produce a stream of data from a function',
            rationale: 'Useful to be able to pass a function into a component that can generate a stream',
            type: 'source',
            meta: {
                length: -1
            }
        })
        return stream
    }

    function createTap () {
        return create({
            name: 'tap',
            type: 'tap',
            description: 'Apply a function for every event passing through the stream',
            rationale: 'Useful to enable side effects'
        })
    }

    function createLog () {
        return create({
            name: 'log',
            type: 'log',
            description: 'Log the event passing through the stream and pass it on',
            rationale: 'Useful to view stream content'
        })
    }

    function createTake () {
        return create({
            name: 'take',
            type: 'take',
            description: 'Allows selection of a subset of events from a stream',
            rationale: 'Useful during debugging to get things right before processing the entire stream'
        })
    }

    function createSkip () {
        return create({
            name: 'skip',
            type: 'skip',
            description: 'Allows selection of a subset of events from a stream',
            rationale: 'Useful during debugging to get things right before processing the entire stream'
        })
    }

    function createEach () {
        return create({
            name: 'each',
            type: 'each',
            description: 'Each item in an event become a separate event',
            rationale: 'Useful when you need to batch up to aggregate but then can process each batch item individually'
        })
    }
    function createBuffer () {
        return create({
            name: 'buffer',
            type: 'buffer',
            description: 'A buffer node will buffer events up to some count of events and then emit all the buffered events.',
            rationale: 'This function is useful to compute statistics on stream values before subsequently using those statistics'
        })
    }
    function createEnd () {
        return create({
            name: 'end',
            type: 'end',
            description: 'An end node will run a function when the stream ends.',
            rationale: 'This function is useful to start some subsequent processing when all data has been seen'
        })
    }
    function createBatch () {
        return create({
            name: 'batch',
            type: 'batch',
            description: 'A batch node will batch events within a time period and up to a max batch size.',
            rationale: 'This function is useful there is a slow process for handling individual events but relatively fast for a set of events (such as DB connections)'
        })
    }

    function createMerge (name = 'merge') {
        return create({
            name,
            type: 'merge',
            // pipeEnabled: false,
            description: 'Events from 2 streams are merged into one stream',
            rationale: 'Useful when there are different sources of data but they are to be processed through some subsequent logic'
        })
    }

    function createJoin (name = 'join') {
        return create({
            name,
            type: 'join',
            description: 'Emits events when data from one stream matches data from another stream',
            rationale: 'Useful when you want to enrich events from events from a second stream'
        })
    }
    function createCombine (name = 'combine') {
        return create({
            name,
            type: 'combine',
            description: 'Emits all combinations from 2 streams',
            rationale: 'Useful when you want all combinations'
        })
    }
    function createTable (name = 'table') {
        return create({
            name,
            type: 'table',
            description: 'Caches records as key-value paris',
            rationale: 'Useful when you need to keep track of state over time'
        })
    }
    function createMap (name = 'map') {
        return create({
            name,
            type: 'map',
            description: 'Transform events through some function',
            rationale: 'Useful in transforming syntax, structure or combining values'
        })
    }
    function createFilter (name = 'filter') {
        return create({
            name,
            type: 'filter',
            description: 'Selects events which match a criteria',
            rationale: 'Useful to restrict data to a subset of the original stream'
        })
    }
    function createTo (name = 'to') {
        return create({
            name,
            type: 'sink',
            description: 'Send events to a stream (file or in memory variable)',
            rationale: 'Useful to store event streams across models'
        })
    }
    function createToChannel () {
        return create({
            name: 'toChannel',
            type: 'toChannel',
            description: 'Events are intended to be handed to infrastructure for communication to subsequent processing',
            rationale: 'Useful to deploy stream processing into multiple independent processes'
        })
    }
    function createToArray () {
        return create({
            name: 'toArray',
            type: 'toArray',
            description: 'Events are captured into an array',
            rationale: 'Useful to test values received at a node'
        })
    }
    function createStreamFromChannel (options = {}) {
        debug(`fromChannel`)
        let stream = create({
            name: options.name ? options.name : 'fromChannel',
            pipeEnabled: false,
            description: 'Produce a stream of data from a channel',
            rationale: 'Useful in forming a pipeline that spans multiple processes',
            type: 'source'
        })
        return stream
    }
    function createReduce () {
        return create({
            name: 'reduce',
            type: 'reduce',
            description: 'Reduce a data stream',
            rationale: 'Useful wanting to accumulate values through a data stream'
        })
    }
    function createPassthrough () {
        return create({
            name: 'passthrough',
            type: 'passthrough',
            description: 'Events pass through this stream',
            rationale: 'Useful when testing functionality in a single process which normally occurs in different processes'
        })
    }
    function createDelay () {
        return create({
            name: 'delay',
            type: 'delay',
            description: 'Propagates events after a delay',
            rationale: 'Useful when modelling activities which take time',
            realworld: 'Modelling the delay of a mining truck receiving a payload and unloading that payload'
        })
    }
    return {
        create,
        createFromChannel,
        createFromCallback,
        createFromLines,
        createFrom,
        createTo,
        createToArray,
        createTap,
        createLog,
        createMerge,
        createFilter,
        createJoin,
        createCombine,
        createTable,
        createMap,
        createReduce,
        createToChannel,
        createPassthrough,
        createDelay,
        createTake,
        createEach,
        createBuffer,
        createEnd
    }
}
