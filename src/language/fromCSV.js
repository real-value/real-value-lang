const debug = require('debug')('from')

module.exports = function (model) {
    const { create } = require('../stream.js')(model)

    return function (content, options = {}) {
        debug(`fromCsv`)

        let streamdata = content.split(',')
        return create({
            name: options.name ? options.name : 'fromCSV',
            meta: {
                length: streamdata.length
            },
            eventSource: () => {
               return model.evaluator().fromCsv(streamdata)
            }
        })
    }
}
