
const debug = require('debug')('from')

module.exports = function (model) {
    const fromCSV = require('./fromCSV.js')(model)

    return function (content, options) {
        debug(`from`)
        if (typeof content === 'string') {
            return fromCSV(content, options)
        }
        return null
    }
}
