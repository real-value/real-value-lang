const debug = require('debug')('model')

const { Writable, Readable } = require('stream')
const { ReadableAdapter, ReadableAdapter2, WriteableAdapter } = require('real-value-stream-adapter')

let Infrastructure = require('real-value-infrastructure-base')

module.exports = function (options = {}) {
    let thesources = []

    let { theInfrastructure = Infrastructure(), highWaterMark = 1000, maxListeners = 10, variables = {}, channelFactory = null } = options
    let shutdownCallback = null

    debug(`model`)

    let theModel = {
        infrastructure,
        run,
        complete,
        sources,
        addSource,
        getNode,
        register,
        deregister,
        highWaterMark,
        maxListeners,
        variables,
        channelFactory,

        fromChannel,
        fromCallback,
        from,
        fromLines,
        tap,
        each,
        merge,
        filter,
        join,
        table,
        map,
        to,
        delay,
        pub,
        passthrough,
        buffer,
        batch
    }

    const { createFromChannel, createFromCallback, createFrom, createFromLines, createTo, createTap, createLog, createEach, createMerge, createFilter, createJoin, createTable, createMap, createPub, createPassthrough, createDelay, createTake, createBuffer, createBatch } = require('./stream.js')(theModel)

    function sources () {
        return thesources
    }
    function addSource (source) {
        return thesources.push(source)
    }
    function infrastructure () {
        return theInfrastructure
    }
    function fromChannel (options) {
        let streamSource = createFromChannel(options)
        addSource(streamSource)
        return streamSource
    }
    function fromCallback (options) {
        let { producer, stream } = createFromCallback(options)
        addSource(stream)
        return { producer, stream }
    }
    function from (content, options) {
        let streamSource = createFrom(content, options)
        addSource(streamSource)
        return streamSource
    }
    function fromLines (content, options) {
        let streamSource = createFromLines(content, options)
        addSource(streamSource)
        return streamSource
    }
    function to (content, options) {
        let streamSource = createTo(null, { file: content })
        addSource(streamSource)
        return streamSource
    }
    function tap (f, options) {
        let streamSource = createTap(f, options)
        addSource(streamSource)
        return streamSource
    }
    function take (count, options) {
        let streamSource = createTake(count, options)
        addSource(streamSource)
        return streamSource
    }
    function buffer (count, options) {
        let streamSource = createBuffer(count, options)
        addSource(streamSource)
        return streamSource
    }
    function batch (count, options) {
        let streamSource = createBatch(count, options)
        addSource(streamSource)
        return streamSource
    }
    function merge (options) {
        let streamSource = createMerge(options)
        addSource(streamSource)
        return streamSource
    }
    function join (options) {
        let streamSource = createJoin(options)
        addSource(streamSource)
        return streamSource
    }
    function table (options) {
        let streamSource = createTable(options)
        addSource(streamSource)
        return streamSource
    }
    function each (options) {
        let streamSource = createEach(options)
        addSource(streamSource)
        return streamSource
    }
    function filter (options) {
        let streamSource = createFilter(options)
        addSource(streamSource)
        return streamSource
    }
    function map (options) {
        let streamSource = createMap(options)
        addSource(streamSource)
        return streamSource
    }
    function pub (options) {
        let streamSource = createPub(options)
        addSource(streamSource)
        return streamSource
    }
    function delay (options) {
        let streamSource = createDelay(options)
        addSource(streamSource)
        return streamSource
    }
    function passthrough (options) {
        let streamSource = createPassthrough(options)
        addSource(streamSource)
        return streamSource
    }

    function setupPipes (streamSource, sourceReadable) {
        let sinks = streamSource.sink()
        for (var j = 0; j < sinks.length; j++) {
            let asink = sinks[j]
            if (asink.pipeEnabled !== false) {
                debug(`Piping from ${streamSource.name} to ${asink.name}`)
                if (asink.duplex instanceof Writable) {
                    let newreadstream = sourceReadable.pipe(asink.duplex)
                    setupPipes(asink, newreadstream)
                } else {
                    sourceReadable.pipe(asink.duplex)
                }
            } else {
                debug(`Skip piping from ${streamSource.name} to ${asink.name}`)
            }
        }
    }
    function run (callback) {
        shutdownCallback = () => {
            callback()
            tidyup()
        }
        for (let i = 0; i < thesources.length; i++) {
            let streamSource = thesources[i]
            let sourceReadable
            if (streamSource.duplex instanceof Readable) {
                sourceReadable = streamSource.duplex
            } else {
                sourceReadable = ReadableAdapter(streamSource.iterator)
            }
            setupPipes(streamSource, sourceReadable)
        }
    }
    function complete () {
        return new Promise((resolve) => {
            shutdownCallback = () => {
                resolve()
                tidyup()
            }
            for (let i = 0; i < thesources.length; i++) {
                let streamSource = thesources[i]
                let sourceReadable
                if (streamSource.duplex instanceof Readable) {
                    sourceReadable = streamSource.duplex
                } else {
                    sourceReadable = ReadableAdapter(streamSource.iterator)
                }
                setupPipes(streamSource, sourceReadable)
            }
        })
    }
    function tidyup () {
        // Allow GC to clean up
        nodes = {}
        nodesByName = {}
        thesources = null
    }

    let nodesByName = {}
    let nodes = {}

    function getNode (name) {
        return nodesByName[name]
    }

    function register (node) {
        debug(`Register ${node.name}-${node.uid}`)
        nodes[`${node.name}-${node.uid}`] = node
        nodesByName[node.name] = node
    }
    function deregister (node) {
        debug(`DeRegister ${node.name}-${node.uid}`)
        delete nodes[`${node.name}-${node.uid}`]
        if (shutdownCallback && Object.keys(nodes).length === 0) {
            debug('Model shutdown')
            shutdownCallback()
        }
    }

    return theModel
}
